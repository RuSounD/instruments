#pragma once

template <typename T>
class Vector
{
	private:
		void expandContainer()
		{
			T *tmp = new T[actualSize];
			for (int i = 0; i < actualSize; i++)
			{
				tmp[i] = container[i];
			}
			delete[]container;
			maxSize += 10;
			container = new T[maxSize];
			for (int i = 0; i < actualSize; i++)
			{
				container[i] = tmp[i];
			}
			delete[]tmp;
		}

		T *container;
		int actualSize;
		int maxSize;
	public:
		void push_back(T value)
		{
			if (actualSize == maxSize)
				expandContainer();
			container[actualSize] = value;
			actualSize++;
		}

		void remove(const int idx)
		{
			if (idx >= actualSize && idx < 0) throw "Incorrect index: not exists";
			if (idx == actualSize)
			{
				actualSize--;
				return;
			}

			for (int i = idx; i < actualSize - 1; i++)
			{
				container[i] = container[i + 1];
			}
			actualSize--;
		}

		int size() const
		{
			return actualSize;
		}
		T operator[](const int idx)
		{
			if (idx >= actualSize && idx < 0) throw "Incorrect index: not exists";
			return container[idx];
		}

		~Vector()
		{
			delete[]container;
		}

		Vector()
		{
			maxSize = 10;
			actualSize = 0;
			container = new T[10];
		}

		Vector(const int size)
		{
			maxSize = size;
			actualSize = 0;
			container = new T[size];
		}
};

